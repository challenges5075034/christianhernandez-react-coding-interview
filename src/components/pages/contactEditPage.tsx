import React from 'react';
import { Button } from '@mui/material';

export function ContactEditPage() {
  const [name, setName] = React.useState('');
  const [email, setEmail] = React.useState('');

  console.log('name', name);

  const submitForm = (e: React.SyntheticEvent) => {
    e.preventDefault();
    console.log('e', e);
    console.log('submit');
  };

  return (
    <div>
      <form onSubmit={submitForm}>
        <input type="text" value={name} onChange={e => setName(e.target.value)} />
        <input type="text" value={email} onChange={e => setEmail(e.target.value)} />
        <Button variant="contained" type="submit" onClick={submitForm}>
          Save
        </Button>
      </form>
    </div>
  );
}
